const FULFILLED = 'fulfilled';
const REJECTED = 'rejected';
const PENDING = 'pending';

class MyPromise {
  #PromiseFulfillReactions = [];
  #PromiseRejectReactions = [];
  #PromiseState = PENDING;
  #PromiseValue = undefined;
  #PromiseCallbacksCall;

  constructor(executor) {
    if (typeof executor !== 'function') {
      throw new TypeError(`${executor} is not a function`);
    }

    this.#PromiseCallbacksCall = () => {
      setTimeout(() => {
        if (this.#PromiseState === FULFILLED) {
          this.#PromiseFulfillReactions.forEach(cb => cb(this.#PromiseValue));
        } else {
          this.#PromiseRejectReactions.forEach(cb => cb(this.#PromiseValue));
        }
      }, 0);
    };

    const reject = reason => {
      if (this.#PromiseState !== PENDING) {
        return;
      }

      this.#PromiseState = REJECTED;
      this.#PromiseValue = reason;
      this.#PromiseCallbacksCall(reason);
    };

    const resolve = value => {
      if (this.#PromiseState !== PENDING) {
        return;
      }

      this.#PromiseState = FULFILLED;
      this.#PromiseValue = value;
      this.#PromiseCallbacksCall(value);
    };

    try {
      executor(resolve, reject);
    } catch (error) {
      reject(error);
    }
  }

  static reject(error) {
    if (this !== MyPromise) {
      throw new TypeError();
    }

    return new MyPromise((res, rej) => rej(error));
  }

  static resolve(value) {
    if (this !== MyPromise) {
      throw new TypeError();
    }

    if (value instanceof MyPromise) {
      return value;
    }
    return new MyPromise(res => res(value));
  }

  static race(arrayPromises) {
    if (this !== MyPromise) {
      throw new TypeError();
    }
    return new MyPromise((res, rej) => {
      arrayPromises.forEach(promise => promise.then(res, rej));
    });
  }

  static all(arrayPromises) {
    if (this !== MyPromise) {
      throw new TypeError();
    }

    if (typeof arrayPromises[Symbol.iterator] !== 'function') {
      return new MyPromise((res, rej) => rej(new TypeError()));
    }

    if (arrayPromises.length === 0) {
      return new MyPromise(res => res(new Array(0)));
    }

    return new MyPromise((res, rej) => {
      const arr = [];

      arrayPromises.forEach(item => {
        item
          .then(value => arr.push(value), error => rej(error))
          .then(() => res(arr));
      });
    });
  }

  then(onResolved, onRejected) {
    if (this.constructor !== MyPromise) {
      throw new TypeError();
    }

    return new MyPromise((res, rej) => {
      if (this.#PromiseState === PENDING) {
        if (typeof onResolved === 'function') {
          this.#PromiseFulfillReactions.push(val => res(onResolved(val)));
        } else {
          this.#PromiseFulfillReactions.push(val => res(val));
        }

        if (typeof onRejected === 'function') {
          this.#PromiseRejectReactions.push(val => res(onRejected(val)));
        } else {
          this.#PromiseRejectReactions.push(val => rej(val));
        }
        return;
      }

      setTimeout(() => {
        if (this.#PromiseState === FULFILLED) {
          if (typeof onResolved === 'function') {
            try {
              res(onResolved(this.#PromiseValue));
            } catch (error) {
              rej(error);
            }
            return;
          }
          res(this.#PromiseValue);
        }

        if (typeof onRejected === 'function') {
          try {
            res(onRejected(this.#PromiseValue));
          } catch (error) {
            rej(error);
          }
          return;
        }

        rej(this.#PromiseValue);
      }, 0);
    });
  }

  catch(cb) {
    return this.then(null, cb);
  }
}

module.exports = MyPromise;
